#include <sys/types.h>          /* some systems still require this */
#include <sys/stat.h>
#include <stdio.h>              /* for convenience */
#include <stdlib.h>             /* for convenience */
#include <stddef.h>             /* for offsetof */
#include <string.h>             /* for convenience */
#include <unistd.h>             /* for convenience */
#include <signal.h>             /* for SIG_ERR */ 
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <ctype.h>

#define BUFLEN 128 
#define QLEN 10 

#ifndef HOST_NAME_MAX 
#define HOST_NAME_MAX 256 
#endif	


//Funcion de ayuda para setear la bandera close on exec
void set_cloexec(int fd){
	if(fcntl(fd, F_SETFD, fcntl(fd, F_GETFD) | FD_CLOEXEC) < 0){
		printf("error al establecer la bandera FD_CLOEXEC\n");	
	}
}


//Funcion para inicializar el servidor
int initserver(int type, const struct sockaddr *addr, socklen_t alen, int qlen){

	int fd;
	int err = 0;
	
	if((fd = socket(addr->sa_family, type, 0)) < 0)
		return -1;
		
	if(bind(fd, addr, alen) < 0)
		goto errout;
		
	if(type == SOCK_STREAM || type == SOCK_SEQPACKET){
		
		if(listen(fd, qlen) < 0)
			goto errout;
	}
	return fd;
errout:
	err = errno;
	close(fd);
	errno = err;
	return (-1);
}


//Damos el servicio
void convertirM(char* mensaje){
	char *ptr=mensaje;
	while(*ptr){
		if(islower(*ptr)){
			*ptr = toupper(*ptr);
		}printf("%c ",*ptr);
		ptr++;	
	}
}
//Main

int main( int argc, char *argv[]) { 
	int sockfd;
	int concurrente = 0;
	
	if(argc == 1){
		printf("Uso: ./servidor <ip> <numero de puerto>\n");
		exit(-1);
	}

	if(argc < 3){
		printf( "por favor especificar el IP y un numero de puerto\n");
	}
	if(argc == 4){
		int n = strcmp(argv[3],"-m");
		if(n==0){
			concurrente++;
			printf("%d\n",n);
		}
	}

	int puerto = atoi(argv[2]);
	

	//Direccion del servidor
	struct sockaddr_in direccion_servidor;

	memset(&direccion_servidor, 0, sizeof(direccion_servidor));	//ponemos en 0 la estructura direccion_servidor

	//llenamos los campos
	direccion_servidor.sin_family = AF_INET;		//IPv4
	direccion_servidor.sin_port = htons(puerto);		//Convertimos el numero de puerto al endianness de la red
	direccion_servidor.sin_addr.s_addr = inet_addr(argv[1]) ;	//Nos vinculamos a la interface localhost o podemos usar INADDR_ANY para ligarnos A TODAS las interfaces

	

	//inicalizamos servidor (AF_INET + SOCK_STREAM = TCP)
	if( (sockfd = initserver(SOCK_STREAM, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor), 1000)) < 0){	//Hasta 1000 solicitudes en cola 
		printf("Error al inicializar el servidor\n");	
	}		
	char buffer[200];
	while(1){
		int clfd = accept(sockfd, NULL, NULL);
		if(clfd < 0){
			printf("ERROR\n");
		}
		if(concurrente == 1 ){
			printf("entro a concurrencia\n");
			pid_t pid = fork();
			if(pid > 0){
				printf("PARENT\n");
				close(clfd);
			}else if(pid == 0){
				printf("CHILD\n");
				recv(clfd,buffer,200,0);
				convertirM(buffer);
				send( clfd, buffer, 200, 0); 
				close(clfd);
			}else{
				printf("Error de fork\n");
			}
		}
		
	}
	

	
	exit( 1); 
}


