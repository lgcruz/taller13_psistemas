all: servidor cliente

servidor: src/servidor.c 
	gcc -Wall -g $^ -o bin/$@

cliente: src/cliente.c 
	gcc -Wall $^ -o bin/$@

clean:
	rm -rf cliente servidor
